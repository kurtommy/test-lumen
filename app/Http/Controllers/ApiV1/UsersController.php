<?php

namespace App\Http\Controllers\ApiV1;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;


class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function createUser(Request $request) {
      $id = DB::table('users')
            ->insertGetId($request->all());
      // $user = DB::table('users')->where('id', $id)->first();
      return response()->json(['created' => true], 201);
    }

    public function userList() {
      $users = DB::table('users')->get();
      return response()->json($users, 200);
    }

    public function getUser($id) {
      $user = DB::table('users')->where('id', $id)->first();
      return response()->json($user, 200);
    }

    public function updateUser(Request $request, $id) {
      DB::table('users')
            ->where('id', $id)
            ->update($request->all());
      return $this->getUser($id);
    }

    public function deleteUser($id) {
      DB::table('users')->where('id', $id)->delete();
      return response()->json(null, 200);
    }
}
