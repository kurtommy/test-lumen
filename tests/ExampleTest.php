<?php

use Laravel\Lumen\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->get('/');

        $this->assertEquals(
            $this->response->getContent(), 'Hello ' . $this->app->version()
        );
    }

    public function testUserPost()
    {
        $this->post('/api/v1/users', ['name' => 'From test'])
             ->seeJson([
                 'created' => true,
             ]);
    }
}
